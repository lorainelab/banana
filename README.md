## Welcome

This repository has data analysis code 
and processed data files from RNA-Seq experiments investigating
gene expression in banana.

Researchers collaborating on this project:

* Ann Loraine, University of North Carolina at Charlotte
* Nowlan Freese, University of North Carolina at Charlotte
* Allan Brown, International Institute of Tropical Agriculture, Arusha

***

## What's here

Folders are data analysis modules 
that are mostly independent but often use files and results from other
modules. These are:

### AltSplicing

What's here:

* Arabitag output files with annotated splice site counts; uses https://bitbucket.org/lorainelab/altspliceanalysis
* Code for detecting and analyzing differential splicing of alternative splice sites
* Lists of differentially spliced genes and splice sites

***

### Counts

* Has alignments per gene counts data, output of featureCounts
* Varies parameters for counting alignments (e.g., multi-mapping or not).
* Creates gene expression tables.
* Depends on GeneRegions

***

### DiffExp

* Contains file for analysis of differential expression
* Contains plain text files listing differentially expressed genes

*** 

### ExternalDataSets

* Contains files imported from external sources, like http://IGBQuickLoad.org.

***

### GeneRegions

* Creates SAF file needed to run featureCounts (Counts)
* Summarizes number of gene models, splice variants
* Shows distribution of gene lengths

***

### src

* Contains data processing scripts
* Contains project-wide R functions, variables

***

# License

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT