#!/bin/bash
#PBS -l nodes=1:ppn=2
#PBS -l vmem=96000mb
#PBS -l walltime=15:00:00
cd $PBS_O_WORKDIR

# -u - use singly-mapped reads only
# -f - require this many bases on flanking exon

J=find-junctions-1.0.0-jar-with-dependencies.jar
T=M_acuminata_DH_Pahang_Jan_2016.2bit

# F passed in from qsub -v option

# requires features from java 8?

module load java8

java -Xmx32g -jar $J -u -f 5 -b $T -o $S.FJ.bed $F
if [ -s "$S.FJ.bed" ]
then
    sort -k1,1 -k2,2n $S.FJ.bed | bgzip > $S.FJ.bed.gz
    rm $S.FJ.bed
    tabix -s 1 -b 2 -e 3 -f -0 $S.FJ.bed.gz
fi