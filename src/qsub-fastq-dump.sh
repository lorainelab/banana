#!/bin/bash

# to run:
#   qsub-fastq-dump.sh >run.out 2>run.err
# to kill jobs:
#   cat run.out | xargs qdel 

SCRIPT="fastq-dump.sh"
SRA="SRP067207"
FS=$(ls $SRA/*.sra)

for F in $FS
do
  S=$(basename $F .sra)
  qsub -N $S -o $S.out -e $S.err -vF=$F $SCRIPT
done
