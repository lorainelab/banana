#!/bin/bash
# featureCounts v1.4.5
# /lustre/groups/lorainelab/sw/subread-1.4.5-Linux-x86_64/bin/featureCounts
# -O count each read for each feaure it overlaps, even if a feature
# overlaps other features 
SAF=$HOME/src/banana/GeneRegions/results/SAF.txt
BASE=counts_mm
OUT=$BASE.txt
ERR=$BASE.err
OUT2=$BASE.out
# include multi-mapping reads
featureCounts -O -M -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2
BASE=counts_sm
OUT=$BASE.txt
ERR=$BASE.err
OUT2=$BASE.out
# don't include multi-mapping reads
featureCounts -O -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2
BASE=counts_pm
OUT=$BASE.txt
ERR=$BASE.err
OUT2=$BASE.out
# use primary alignment only
featureCounts -O --primary -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2
