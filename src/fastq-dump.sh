#!/bin/bash
#PBS -l walltime=8:00:00
#PBS -l nodes=1:ppn=1
#PBS -l vmem=8000mb

# F passed from qsub-fastq-dump.sh
cd $PBS_O_WORKDIR
module load sratoolkit/2.3.3
fastq-dump --gzip $F

