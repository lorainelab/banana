#!/bin/bash

# to run: 
#   - make arabitag directory
#   - make add links to gene models, FJ files
#   - run with 96 Gb memory (less may be OK)
#
# run in subdirectory of processed BAM files

# gene model file; BED file from IGBQuickload.org, minus
# fields 13 and 14
B=M_acuminata_DH_Pahang_Jan_2016.bed
S=dry_root
F=5
RI=10
if ! [ -s allJunctions_$S.bed ]; then
    PreParse.py -s _$S *FJ.bed.gz
fi
if ! [ -s AltSplicing_$S.abt ]; then
    ArabiTagMain.py -g $B -e allJunctions_$S.bed -s $S -f $F
fi
if ! [ -s AltSplicing_$S.f$F.AS.txt ]; then
    PostParse.py -b $B AltSplicing_$S.abt AltSplicing_$S.f$F.AS.txt
fi
if ! [ -s AltSplicing_$S.f$F.ASwiRI.txt ]; then
    Add_RiGp_Support.py -o AltSplicing_$S.f$F.ASwiRI.txt -i AltSplicing_$S.f$F.AS.txt -f $RI -d ..
fi