#!/bin/bash
#PBS -l nodes=1:ppn=8
#PBS -l vmem=96000mb
#PBS -l walltime=8:00:00
cd $PBS_O_WORKDIR

# O, S, G defined by qsub -v 
tophat -p 8 -I 5000 -o $O/$S $G $S.fastq.gz

