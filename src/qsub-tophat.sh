#!/bin/bash

# to run: 
#    qsub-tophat.sh >jobs.out 2>jobs.err
# to kill:
#    cat jobs.out | xargs qdel 

SCRIPT=tophat.sh
G=M_acuminata_DH_Pahang_Jan_2016
O=SRP067207

FS=$(ls *.fastq.gz)
for F in $FS
do
    S=$(basename $F .fastq.gz)
    CMD="qsub -N $S -o $S.out -e $S.err -vS=$S,O=$O,G=$G $SCRIPT"
    $CMD
done


